# Tasks-manager

Tasks-manager is a Spring-boot application



## How to run application without docker

1. Build Jar
   * go to the root directory of the project
   * If you have gradle installed run: `gradle assemble`
   * If you don't have gradle installed rrun: `gradlew assemble`
   * go check if the .jar have been created build -> libs -> gs-spring-boot-0.1.0.jar
2. Run the application
   * run the following command: `java -jar gs-spring-boot-0.1.0.jar`