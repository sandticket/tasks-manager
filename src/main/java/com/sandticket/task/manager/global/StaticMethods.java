package com.sandticket.task.manager.global;

import com.sandticket.task.manager.elasticsearch.SearchHitIterator;
import com.sandticket.task.manager.model.Task;
import com.sandticket.task.manager.model.User;
import org.elasticsearch.search.SearchHit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class StaticMethods {

    public static ArrayList<Task> BindTaskResult(SearchHitIterator hitIterator){
        ArrayList<Task> taskList = new ArrayList<>();
        while (hitIterator.hasNext()) {
            System.out.println("hitIterator +1");
            SearchHit hit = hitIterator.next();
            taskList.add(BindSearchHitToTask(hit));
        }
        return taskList;
    }

    public static Task BindSearchHitToTask(SearchHit searchHit) {
        Task task = new Task();
        task.setTitle(searchHit.getSourceAsMap().get("title").toString());
        task.setDescription(searchHit.getSourceAsMap().get("description").toString());
        task.setLastUpdateDate(searchHit.getSourceAsMap().get("lastUpdateDate").toString());
        task.setDueDate(searchHit.getSourceAsMap().get("dueDate").toString());
        task.setUuid(searchHit.getSourceAsMap().get("uuid").toString());
        // task.setStatus((Task.Status) hit.getSourceAsMap().get("status"));
        return task;
    }

    public static ArrayList<User> BindUserResult(SearchHitIterator hitIterator){
        ArrayList<User> userList = new ArrayList<>();
        while (hitIterator.hasNext()) {
            User user = new User();
            SearchHit hit = hitIterator.next();
            user.setFirstName(hit.getSourceAsMap().get("firstName").toString());
            user.setLastname(hit.getSourceAsMap().get("lastname").toString());
            user.setEmail(hit.getSourceAsMap().get("email").toString());

            // task.setStatus((Task.Status) hit.getSourceAsMap().get("status"));
            userList.add(user);
        }
        return userList;
    }

    public static String getImportTrelloProject(String url) throws IOException {

        String source ="";
        URL oracle = new URL(url);
        URLConnection yc = oracle.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        yc.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            source +=inputLine;
        in.close();
        return source;
    }
}
