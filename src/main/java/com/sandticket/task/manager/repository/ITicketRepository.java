package com.sandticket.task.manager.repository;

import com.sandticket.task.manager.model.ImportTrelloProjectResponse;
import com.sandticket.task.manager.model.Task;

import java.io.IOException;
import java.util.List;

public interface ITicketRepository {
    List<Task> findAllTickets();
    Task findTicketById(String taskId);
    List<Task> findTicketByTitle(String title) throws IOException;
    List<Task> findTicketByDescription(String description);
    List<Task> findByCreationDate(String creationDate);
    List<Task> findByDueDate (String dueDate);
    List<Task> findByLastUpdateDate (String lastUpdateDate);
    List<Task> findByStatus (String taskStatus);
    Task deleteTicket(String ticketId);
    ImportTrelloProjectResponse getProjectFromTrello(String projectId) throws IOException;
}
