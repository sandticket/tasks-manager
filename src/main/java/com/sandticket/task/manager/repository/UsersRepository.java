package com.sandticket.task.manager.repository;

import com.sandticket.task.manager.elasticsearch.SearchHitIterator;
import com.sandticket.task.manager.model.User;
import com.sandticket.task.manager.service.UsersService;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.sandticket.task.manager.global.StaticMethods.BindUserResult;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

@Repository
public class UsersRepository implements IUsersRepository {

    @Autowired
    Client client;

    @Override
    public List<User> getUsers() {
        List<User> userList;

        SearchRequestBuilder requestBuilder = client.prepareSearch("users")
                .setTypes("_doc")
                .setQuery(QueryBuilders.matchAllQuery());

        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        //userList = BindUserResult(hitIterator);
        displayResultSearch(hitIterator);

        return null;
    }

    private static void displayResultSearch(SearchHitIterator hitIterator) {
        while (hitIterator.hasNext()) {
            SearchHit hit = hitIterator.next();
            System.out.println("hit.docId(): " + hit.getId());
            System.out.println("hit.getSourceAsString(): " + hit.getSourceAsString());
            System.out.println("hit fields :"+hit.getFields());
            System.out.println("hit sources as map "+hit.getSourceAsMap().get("firstname"));
        }
    }

    @Override
    public User findUserById(@PathVariable String usersId) {

        //Task task = new Task();
        if(client != null ){
            SearchRequestBuilder requestBuilder = client.prepareSearch("users")
                    .setTypes("_doc")
                    .setQuery(QueryBuilders.matchAllQuery());

            SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
            while (hitIterator.hasNext()) {
                SearchHit hit = hitIterator.next();
                if(usersId.equals(hit.getId())){
                    System.out.println("OK");
                }
                else{
                    System.out.println("KO");
                    System.out.println("user id : "+usersId);
                    System.out.println("hit get id "+hit.getId());
                }
            }
        }

        return null;
    }

    @Override
    public List<User> findUserByFirstname(String user_firstname){
        List<User> userList;
        SearchRequestBuilder requestBuilder = client.prepareSearch("users")
                .setTypes("_doc")
                .setQuery(QueryBuilders.termQuery("firstName",user_firstname));
        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        userList = BindUserResult(hitIterator);
        return userList;
    }

    @Override
    public List<User> findUserByLastname(@PathVariable String user_lastname){
        List<User> userList;
        SearchRequestBuilder requestBuilder = client.prepareSearch("users")
                .setTypes("_doc")
                .setQuery(QueryBuilders.termQuery("lastname",user_lastname));
        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        userList = BindUserResult(hitIterator);
        return userList;
    }

    @Override
    public List<User> findUserByEmail(@PathVariable String user_email){
        List<User> userList;
        SearchRequestBuilder requestBuilder = client.prepareSearch("users")
                .setTypes("_doc")
                .setQuery(QueryBuilders.termQuery("email",user_email));
        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        userList = BindUserResult(hitIterator);
        return userList;
    }

    @Override
    public User deleteUser(@PathVariable String userId) {
        DeleteResponse response = client.prepareDelete("users","_doc", userId).get();
        if (null == response){
            System.out.println("La suppression n\'a pas fonctionnée");
        }
        return null;
    }

    @Override
    public Map<String, Object> createUser(HttpServletRequest request) {
        Enumeration enumeration = request.getParameterNames();
        Map<String, Object> modelMap = new HashMap<>();
        IndexRequestBuilder indexRequestBuilder = client.prepareIndex("users", "_doc");
        while(enumeration.hasMoreElements()){
            String parameterName = enumeration.nextElement().toString();
            modelMap.put(parameterName, request.getParameter(parameterName));
            try {
                indexRequestBuilder.setSource(jsonBuilder().startObject().field(parameterName, request.getParameter(parameterName)).endObject());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        IndexResponse response = indexRequestBuilder.get();

        if(null == response)
        {
            return null;
        }
        return modelMap;
    }
}
