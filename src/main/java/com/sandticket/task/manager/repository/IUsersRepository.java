package com.sandticket.task.manager.repository;

import com.sandticket.task.manager.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface IUsersRepository {
    List<User> getUsers();
    User findUserById(String usersId);
    List<User> findUserByFirstname(String user_firstname);
    List<User> findUserByLastname(String user_lastname);
    List<User> findUserByEmail(String user_email);
    User deleteUser(String userId);
    Map<String, Object> createUser(HttpServletRequest request);
}
