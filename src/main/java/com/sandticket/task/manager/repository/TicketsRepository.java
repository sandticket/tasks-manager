package com.sandticket.task.manager.repository;

import com.google.gson.Gson;
import com.sandticket.task.manager.elasticsearch.SearchHitIterator;
import com.sandticket.task.manager.global.StaticMethods;
import com.sandticket.task.manager.model.CreateTicketResponse;
import com.sandticket.task.manager.model.ImportTrelloProjectResponse;
import com.sandticket.task.manager.model.Task;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.ActionFuture;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.*;

import static com.sandticket.task.manager.global.StaticMethods.BindTaskResult;
import static com.sandticket.task.manager.global.StaticMethods.getImportTrelloProject;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

@Repository
public class TicketsRepository implements ITicketRepository {

    Logger logger = LoggerFactory.getLogger(TicketsRepository.class);

    @Autowired
    Client client;

    @Override
    public List<Task> findAllTickets() {
        List<Task> taskList;
        SearchRequestBuilder requestBuilder = client.prepareSearch("tasks")
                .setTypes("_doc")
                .setQuery(QueryBuilders.matchAllQuery());

        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        taskList = BindTaskResult(hitIterator);
        return taskList;
    }

    @Override
    public Task findTicketById(String taskId) {
        //Task task = new Task();
        if(client != null ){
            SearchRequestBuilder requestBuilder = client.prepareSearch("tasks")
                    .setTypes("_doc")
                    .setQuery(QueryBuilders.matchAllQuery());

            SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
            while (hitIterator.hasNext()) {
                SearchHit hit = hitIterator.next();
                if(taskId.equals(hit.getId())){
                    return StaticMethods.BindSearchHitToTask(hit);
                }
            }
        }

        return null;
    }

    @Override
    public List<Task> findTicketByTitle(String title) throws IOException {
        List<Task> taskList;
        SearchRequestBuilder requestBuilder = client.prepareSearch("tasks")
                .setTypes("_doc")
                .setQuery(QueryBuilders.termQuery("title",title));
        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        taskList = BindTaskResult(hitIterator);
        return taskList;
    }

    @Override
    public List<Task> findTicketByDescription(String description) {
        List<Task> taskList;
        SearchRequestBuilder requestBuilder = client.prepareSearch("tasks")
                .setTypes("_doc")
                .setQuery(QueryBuilders.termQuery("description",description));
        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        taskList = BindTaskResult(hitIterator);
        return taskList;
    }

    @Override
    public List<Task> findByCreationDate(String creationDate) {
        List<Task> taskList;
        SearchRequestBuilder requestBuilder = client.prepareSearch("tasks")
                .setTypes("_doc")
                .setQuery(QueryBuilders.termQuery("creationDate",creationDate));
        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        taskList = BindTaskResult(hitIterator);
        return taskList;
    }

    @Override
    public List<Task> findByDueDate(String dueDate) {
        List<Task> taskList;
        SearchRequestBuilder requestBuilder = client.prepareSearch("tasks")
                .setTypes("_doc")
                .setQuery(QueryBuilders.termQuery("dueDate", dueDate));
        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        taskList = BindTaskResult(hitIterator);
        return taskList;
    }

    @Override
    public List<Task> findByLastUpdateDate(String lastUpdateDate) {
        List<Task> taskList;
        SearchRequestBuilder requestBuilder = client.prepareSearch("tasks")
                .setTypes("_doc")
                .setQuery(QueryBuilders.termQuery("lastUpdateDate", lastUpdateDate));
        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        taskList = BindTaskResult(hitIterator);
        return taskList;
    }

    @Override
    //need to be fixed
    public List<Task> findByStatus(String taskStatus) {
        List<Task> taskList;
        SearchRequestBuilder requestBuilder = client.prepareSearch("tasks")
                .setTypes("_doc")
                .setQuery(QueryBuilders.termQuery("status", taskStatus));
        SearchHitIterator hitIterator = new SearchHitIterator(requestBuilder);
        taskList = BindTaskResult(hitIterator);
        return taskList;
    }

    private static void displayResultSearch(SearchHitIterator hitIterator) {
        while (hitIterator.hasNext()) {
            SearchHit hit = hitIterator.next();
        }
    }



    public Task deleteTicket(String ticketId) {
        DeleteResponse response = client.prepareDelete("tasks","_doc", ticketId).get();
        if (null == response){
            System.out.println("La suppression n\'a pas fonctionnée");
        }
        return null;
    }

    public CreateTicketResponse createTicket(Task request) {
        request.setUuid(request.getUuid() == null ? UUID.randomUUID().toString() : request.getUuid());
        IndexRequest indexRequest = new IndexRequest("tasks", "_doc", request.getUuid())
                .source(request.toMap());
        try {
            ActionFuture<IndexResponse> response = client.index(indexRequest);
            logger.info("createTicket | response: " + String.valueOf(response.actionGet().status()));
        } catch(ElasticsearchException e) {
            logger.info("createTicket | exception: " + String.valueOf(e.status()));
        }
        return new CreateTicketResponse(request.getUuid());
    }

    public ImportTrelloProjectResponse getProjectFromTrello(String projectId) throws IOException {
        String trelloServiceBaseUrl = System.getenv("TRELLO_SERVICE_URL");
        String trelloServicePort = System.getenv("TRELLO_SERVICE_PORT");
        String retourDeLaPage = getImportTrelloProject( "http://" + trelloServiceBaseUrl + ":" + trelloServicePort + "/import/" + projectId);
        return new Gson().fromJson(retourDeLaPage, ImportTrelloProjectResponse.class);
    }


}
