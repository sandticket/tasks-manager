package com.sandticket.task.manager.controller;


import com.sandticket.task.manager.model.User;
import com.sandticket.task.manager.service.UsersService;
import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")

public class UsersRestController {

    private UsersService usersService;

    public UsersRestController(final UsersService usersService){
        this.usersService = usersService;
    }


    @GetMapping("")
    public List<User> getUsers() {
        return this.usersService.findAllUsers();
    }

    @GetMapping("/usersId={userId}")
    public User findTicketById(@PathVariable String userId) {
        return this.usersService.findUserById(userId);
    }

    @GetMapping("/firstname={firstname}")
    public List<User> findByFirstname(@PathVariable String firstname){
        return this.usersService.findUserByFirstname(firstname);
    }

    @GetMapping("/lastname/{lastname}")
    public List<User> findByDescription(@PathVariable String lastname){
        return this.usersService.findUserByLastname(lastname);
    }

    @GetMapping("/email/{email}")
    public List<User> findByCreationDate(@PathVariable String email){
        return this.usersService.findUserByEmail(email);
    }

    @GetMapping("/delete/{userId}")
    public User deleteTicket(@PathVariable String userId) {
        return this.usersService.deleteUser(userId);
    }

    @GetMapping("/create")
    public Map<String, Object> createTicket(HttpServletRequest request) {
        Map<String, Object> response = null;
        response = this.usersService.createUser(request);
        if (null == response) {
            System.out.println("La création d'un user a échouée");
            return null;
        }else{
            return response;
        }
    }

}
