package com.sandticket.task.manager.controller;

import com.sandticket.task.manager.model.CreateTicketResponse;
import com.sandticket.task.manager.model.Task;
import com.sandticket.task.manager.repository.TicketsRepository;
import com.sandticket.task.manager.service.TicketsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.tags.Param;

import java.io.IOException;
import java.util.*;


@RestController
@RequestMapping(value="/tasks",method={RequestMethod.POST,RequestMethod.GET})

public class TicketsRestController {

    private Logger logger = LoggerFactory.getLogger(TicketsRepository.class);

    private TicketsService ticketsService;

    public TicketsRestController(final TicketsService ticketsService){
        this.ticketsService = ticketsService;
    }


    @GetMapping("")
    public List<Task> getTickets(@RequestParam(required = false) String title,
                                 @RequestParam(required = false) String description,
                                 @RequestParam(required = false) String createdAt,
                                 @RequestParam(required = false) String dueAt,
                                 @RequestParam(required = false) String lastUpdateDate,
                                 @RequestParam(required = false) String taskStatus,
                                 @RequestParam(required = false)String taskId) throws IOException {
        if(title != null)
            return this.ticketsService.findTicketByTitle(title);
        if(description != null)
            return this.ticketsService.findTicketByDescription(description);
        if(createdAt != null)
            return this.ticketsService.findByCreationDate(createdAt);
        if(lastUpdateDate != null)
            return this.ticketsService.findByLastUpdateDate(lastUpdateDate);
        if(dueAt != null)
            return this.ticketsService.findByDueDate(dueAt);
        if(taskStatus != null)
            return this.ticketsService.findByStatus(taskStatus);

        return this.ticketsService.findAllTickets();
    }

    @GetMapping("/delete/{ticketId}")
    public Task deleteTicket(@PathVariable String ticketId) {
        return this.ticketsService.deleteTicket(ticketId);
    }

    @PostMapping("/create")
    public CreateTicketResponse createTicket(@RequestBody Task request) {
        CreateTicketResponse response = null;
        response = this.ticketsService.createTicket(request);
        if (null == response) {
            logger.info("La création d'une task a échouée");
            return null;
        }else{
            return response;
        }
    }

    @GetMapping("/import/{projectId}")
    public void importFromTrello(@PathVariable String projectId) {
        this.ticketsService.importFromTrello(projectId);
    }
}

