package com.sandticket.task.manager.service;

import com.sandticket.task.manager.model.CreateTicketResponse;
import com.sandticket.task.manager.model.ImportTrelloProjectResponse;
import com.sandticket.task.manager.model.Task;
import com.sandticket.task.manager.repository.TicketsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class TicketsService implements ITicketsService {
    private TicketsRepository ticketsRepository;

    @Autowired
    public TicketsService(final TicketsRepository ticketsRepository){
        this.ticketsRepository = ticketsRepository;
    }

    @Override
    public List<Task> findAllTickets() {
        return this.ticketsRepository.findAllTickets();
    }

    @Override
    public Task findTicketById(String taskId) {
        return this.ticketsRepository.findTicketById(taskId);
    }

    @Override
    public List<Task> findTicketByTitle(String title) throws IOException {
        return this.ticketsRepository.findTicketByTitle(title);
    }

    @Override
    public List<Task> findTicketByDescription(String description) {
        return this.ticketsRepository.findTicketByDescription(description);
    }

    @Override
    public List<Task> findByCreationDate(String creationDate) {
        return this.ticketsRepository.findByCreationDate(creationDate);
    }

    @Override
    public List<Task> findByDueDate(String dueDate) {
        return this.ticketsRepository.findByDueDate(dueDate);
    }

    @Override
    public List<Task>findByLastUpdateDate(String lastUpdateDate) {
        return this.ticketsRepository.findByLastUpdateDate(lastUpdateDate);
    }

    @Override
    public List<Task> findByStatus(String taskStatus) {
        return this.ticketsRepository.findByStatus(taskStatus);
    }

    public Task deleteTicket(String ticketId) {
        return this.ticketsRepository.deleteTicket(ticketId);
    }

    @Override
    public void importFromTrello(String projectId) {
        try {
            ImportTrelloProjectResponse projectFromTrello = this.ticketsRepository.getProjectFromTrello(projectId);
            List<Task> tasks = projectFromTrello.getTasks();
            for (Task task : tasks) {
                task.setCreator(null);
                this.createTicket(task);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CreateTicketResponse createTicket(Task request) {
        return this.ticketsRepository.createTicket(request);
    }
}
