package com.sandticket.task.manager.service;

import com.sandticket.task.manager.model.Task;

import java.io.IOException;
import java.util.List;

public interface ITicketsService {
    List<Task> findAllTickets();
    Task findTicketById(String taskId);
    List<Task> findTicketByTitle(String title) throws IOException;
    List<Task> findTicketByDescription(String description);
    List<Task> findByCreationDate(String creationDate);
    List<Task> findByDueDate (String dueDate);
    List<Task> findByLastUpdateDate (String lastUpdateDate);
    List<Task> findByStatus (String taskStatus);
    Task deleteTicket(String ticketId);
    void importFromTrello(String projectId);
}
