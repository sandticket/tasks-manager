package com.sandticket.task.manager.service;

import com.sandticket.task.manager.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface IUsersService {
    List<User> findAllUsers();
    User findUserById(String userId);
    List<User> findUserByFirstname(String firstname);
    List<User> findUserByLastname(String lastname);
    List<User> findUserByEmail(String email);
    User deleteUser(String UserId);
    Map<String, Object> createUser(HttpServletRequest request);
}
