package com.sandticket.task.manager.service;

import com.sandticket.task.manager.model.User;
import com.sandticket.task.manager.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Service
public class UsersService implements IUsersService{
    private UsersRepository usersRepository;

    @Autowired
    public UsersService(final UsersRepository usersRepository){
        this.usersRepository = usersRepository;
    }

    @Override
    public List<User> findAllUsers() {
        return this.usersRepository.getUsers();
    }

    @Override
    public User findUserById(String taskId) {
        return this.usersRepository.findUserById(taskId);
    }

    @Override
    public List<User> findUserByFirstname(String firstname) {
        return this.usersRepository.findUserByFirstname(firstname);
    }

    @Override
    public List<User> findUserByLastname(String lastname) {
        return this.usersRepository.findUserByLastname(lastname);
    }

    @Override
    public List<User> findUserByEmail(String email) {
        return this.usersRepository.findUserByEmail(email);
    }
    @Override
    public User deleteUser(String ticketId) {
        return this.usersRepository.deleteUser(ticketId);
    }

    @Override
    public Map<String, Object> createUser(HttpServletRequest request) {
        Map<String, Object> response = null;
        response = this.usersRepository.createUser(request);
        if (null == response) {
            System.out.println("La création d'un user a échouée");
            return null;
        }else{
            return response;
        }
    }

}
