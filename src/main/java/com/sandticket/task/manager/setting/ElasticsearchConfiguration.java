package com.sandticket.task.manager.setting;

import com.sandticket.task.manager.service.TicketsService;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
public class ElasticsearchConfiguration {
    @Value("${elasticsearch.host}")
    public String host;
    @Value("${elasticsearch.port}")
    public int port;
    public String getHost() {
        return host;
    }
    public int getPort() {
        return port;
    }
    @Bean
    public Client client(){
        TransportClient client = null;
        try{
            Settings settings = Settings.builder()
                    .put("cluster.name", "docker-cluster")
                    .put("client.transport.ignore_cluster_name", false)
                    .build();

            client = new PreBuiltTransportClient(settings)
                    .addTransportAddress(new TransportAddress(InetAddress.getByName(host), port));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return client;
    }
}
