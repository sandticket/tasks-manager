package com.sandticket.task.manager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    private String uuid;
    private String title;
    private String description;
    private String creationDate;
    private String dueDate;
    private String lastUpdateDate;
    private Status status;
    private User creator;

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid);
        map.put("title", title);
        map.put("description", description);
        map.put("creationDate", creationDate);
        map.put("dueDate", dueDate);
        map.put("lastUpdateDate", lastUpdateDate);
        map.put("status", status);
        map.put("creator", creator);
        return map;
    }

    public enum Status {
        CLOSED,
        NOT_CLOSED
    }
    public String getConvertedStatus(Status status){
        if(status.equals(Status.CLOSED))
            return "CLOSED";
        else if (status.equals(Status.NOT_CLOSED))
            return "NOT_CLOSED";
        return "";
    }

    public Status setConvertedStatus(String s) {
        if(s.equals("CLOSED"))
            return Status.CLOSED;
        else if(s.equals("NOT_CLOSED"))
            return  Status.NOT_CLOSED;
        return Status.CLOSED;
    }
}
